import { get } from "./apiHelper";

const endpoint = "https://run.mocky.io/v3/aa1e2689-38bc-4594-91f4-6e344ede8ec8";

export const getUsers = () => {
  return get(endpoint, false);
};
