import axios from 'axios';

export async function post(endpoint, auth, body) {
  const headers = await getHeaders(auth);
  return axios.post(endpoint, body, { headers });
};

export async function get(endpoint, auth) {
  const headers = await getHeaders(auth);
  return axios.get(endpoint, { headers });
};

export async function put(endpoint, auth, body) {
  const headers = await getHeaders(auth);
  return axios.put(endpoint, body, { headers });
};

export async function del(endpoint, auth) {
  const headers = await getHeaders(auth);
  return axios.delete(endpoint, { headers });
};

export async function getHeaders(auth) {
  let headers = {
    'content-type': 'application/json',
    accept: 'application/json',
  };
  if (auth) {
    const token = "token"; //await AsyncStorage.getItem(USER_TOKEN_KEY);
    headers = Object.assign({ Authorization: 'Bearer ' + token }, headers);
  }
  return headers;
};
