import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { List } from "../screens/List";
import { UserDetailsScreen } from "../screens/UserDetailsScreen";

const MainStack = createStackNavigator();

export const Main = () => (
  <MainStack.Navigator>
    <MainStack.Screen name="List" component={List} />
    <MainStack.Screen
      name="UserDetails"
      component={UserDetailsScreen}
      options={{ headerTitle: "User Details" }}
    />
  </MainStack.Navigator>
);
