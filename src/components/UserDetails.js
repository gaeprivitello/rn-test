import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";

import colors from "../constants/colors";

export const UserDetails = ({ user }) => {
  return (
    <View style={styles.container}>
      <View style={styles.containerCentered}>
        <Image
          style={styles.logo}
          source={{
            uri: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg==",
          }}
        />
        <Text style={styles.header}>{`${user.FirstName} ${user.LastName}`}</Text>
      </View>

      <View style={{ flex: 4 }}>
        <Text style={styles.subheader}>{`ID: ${user.id}`}</Text>
        <Text style={styles.subheader}>{`Email: ${user.Email}`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 6,
    backgroundColor: colors.white,
    padding: 10,
  },
  containerCentered: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
  },
  header: {
    fontSize: 20,
    fontWeight: "bold",
  },
  subheader: {
    fontSize: 14,
  },
  logo: {
    flex: 0.5,
    width: 200,
    height: 200,
    borderColor: "grey",
    borderWidth: 2,
    borderRadius: 100,
  },
});
