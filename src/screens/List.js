import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList } from 'react-native';

import colors from '../constants/colors';
import { ListItem, ListSeparator } from '../components/List';
import { getUsers } from '../api/usersApi';

export const List = ({ navigation }) => {

  const [users,setUsers] = useState([]);

  useEffect(() => {
    getUsers().then((res) => {
      setUsers(res.data);
    });
  },[]);

  return (
    <FlatList
      style={styles.container}
      data={users}
      keyExtractor={item => item.Email}
      renderItem={({ item }) => (
        <ListItem
          title={item.FirstName}
          subtitle={item.LastName}
          onPress={() => navigation.push("UserDetails", { user: item })}
        />
      )}
      ItemSeparatorComponent={ListSeparator}
      ListHeaderComponent={ListSeparator}
      ListFooterComponent={ListSeparator}
      maxToRenderPerBatch={5}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    paddingVertical: 20,
  },
});
