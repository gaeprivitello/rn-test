import React from "react";
import { StyleSheet, View } from "react-native";
import { UserDetails } from "../components/UserDetails";

import colors from "../constants/colors";

export const UserDetailsScreen = ({ route, navigation }) => {
  const { user } = route.params;

  return (
    <View style={styles.container}>
      <UserDetails user={user} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 10,
  },
});
